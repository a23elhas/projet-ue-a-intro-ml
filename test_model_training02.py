import unittest
import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import StratifiedKFold
from code_test02 import train_and_tune_model  

class TestTrainAndTuneModel(unittest.TestCase):

    def setUp(self):
        
        self.X_train = pd.DataFrame({
            'feature1': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            'feature2': [10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
            'feature3': [5, 4, 3, 2, 1, 1, 2, 3, 4, 5]
        })
        self.y_train = np.array([0, 1, 0, 1, 0, 1, 0, 1, 0, 1])

        
        self.model = RandomForestClassifier()
        self.param_grid = {
            'n_estimators': [10, 50],
            'max_depth': [None, 5]
        }
        self.cv = StratifiedKFold(n_splits=3)  

    def test_train_and_tune_model_default(self):
        
        best_model = train_and_tune_model(
            self.X_train, self.y_train, self.model, self.param_grid, self.cv
        )

        
        self.assertIsNotNone(best_model, "The function did not return a model.")
        self.assertTrue(hasattr(best_model, "fit"), "The returned object is not a valid model.")

    def test_train_and_tune_model_return_full_search(self):
        
        best_model, grid_search = train_and_tune_model(
            self.X_train, self.y_train, self.model, self.param_grid, self.cv, return_full_search=True
        )

        self.assertIsNotNone(best_model, "The function did not return a model.")
        self.assertTrue(hasattr(best_model, "fit"), "The returned object is not a valid model.")
        self.assertIsNotNone(grid_search, "The function did not return a GridSearchCV object.")
        self.assertTrue(hasattr(grid_search, "best_score_"), "The returned GridSearchCV object is not valid.")

    def test_train_and_tune_model_scoring(self):
        
        best_model = train_and_tune_model(
            self.X_train, self.y_train, self.model, self.param_grid, self.cv, scoring="accuracy"
        )

        
        self.assertIsNotNone(best_model, "The function did not return a model.")
        self.assertTrue(hasattr(best_model, "fit"), "The returned object is not a valid model.")


if __name__ == "__main__":
    unittest.main()
