import unittest
from unittest.mock import patch, mock_open
import pandas as pd
import numpy as np
from code_test02 import load_data, column_label, preprocessing  

class TestPreprocessingFunctions(unittest.TestCase):

    
    @patch("builtins.open", new_callable=mock_open, read_data="col1,col2,col3\n1,2,3\n4,5,6\n7,8,9")
    def test_load_data(self, mock_file):
        
        data = load_data("mock_path.csv")
        
        
        self.assertIsInstance(data, pd.DataFrame)  
        self.assertEqual(data.shape, (3, 3))  

    
    @patch("builtins.open", new_callable=mock_open, read_data="1,4\n2,5\n3,6")
    def test_column_label(self, mock_file):
        
        result = column_label("mock_path.csv")
        
        
        self.assertEqual(result.shape, (3, 2))  

    
    @patch("builtins.open", new_callable=mock_open, 
           read_data="""id,age,gender,dm,cad,classification
                        1,25,Male,yes,no,ckd
                        2,,Female,no,no,ckd
                        3,35,,yes,no,notckd
                        4,45,Male,yes,no,ckd
                        5,50,Male,no,no,notckd""")
    def test_preprocessing(self, mock_file):
        
        processed_data, target = preprocessing("mock_path.csv")

        
        self.assertNotIn("id", processed_data.columns)

        
        self.assertFalse(processed_data.isnull().any().any())

        
        self.assertTrue(all(val in [0, 1] for val in target))

        
        numerical_cols = ['age']  
        for col in numerical_cols:
            if col in processed_data.columns:
                self.assertAlmostEqual(processed_data[col].mean(), 0, places=5)

        
        for col in processed_data.columns:
            self.assertFalse('.' in col or '_' in col)  

        
        self.assertEqual(len(processed_data.columns), len(set(processed_data.columns)))


if __name__ == "__main__":
    unittest.main()
